#!/bin/bash

if [ -z "$1" ]; then echo "usage: docker-nsenter <container_name> | <container_id>"; exit; fi

ContainerPID=$(docker inspect --format {{.State.Pid}} $1)

if [ -z "$ContainerPID" ]; then exit; fi

echo "Container PID: $ContainerPID"

echo "Entering container \"$1\""

sudo nsenter --target $ContainerPID --mount --uts --ipc --net --pid