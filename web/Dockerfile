FROM php:5.6-apache
# https://hub.docker.com/_/php/

MAINTAINER Zdenko Gradecak <zdenko.gradecak@gmail.com>

RUN apt-get update && apt-get install -y --no-install-recommends \
    sudo \
    nano \
    git \
    zlib1g-dev \
    libicu-dev \
    libxml2-dev \
    openssh-server \
    rsync \
    && rm -rf /var/lib/apt/lists/*


#
# PHP
#

## Install PHP extensions
RUN docker-php-ext-install zip
RUN docker-php-ext-install mysql
RUN docker-php-ext-install mysqli
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install intl
RUN docker-php-ext-install xml

#
# Apache
#

## Enable mod rewrite
RUN a2enmod rewrite

## Enable ssl
RUN a2enmod ssl

## Copy SSL certs
COPY apache/ssl-certs/apache.pem /etc/ssl/certs/ssl-cert-snakeoil.pem
COPY apache/ssl-certs/apache.key /etc/ssl/private/ssl-cert-snakeoil.key


#
# Symfony 3:
#

## Install symfony installer:
RUN curl -LsS https://symfony.com/installer -o /usr/local/bin/symfony
RUN chmod a+x /usr/local/bin/symfony

## Adding php cli command alias to run as www-data user:
RUN echo '#!/bin/bash\nsudo -u www-data php "$@"' > /usr/bin/phpwww
RUN chmod +x /usr/bin/phpwww


#
# Misc
#

## Adding ll command alias
RUN echo '#!/bin/bash\nls -la --color' > /usr/bin/ll
RUN chmod +x /usr/bin/ll

EXPOSE 80 443
